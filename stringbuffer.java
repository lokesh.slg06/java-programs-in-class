import java.lang.*;

 class StringBufferDemo {

   public static void main(String[] args) {
	// Append
      StringBuffer buff = new StringBuffer("tuts ");
      System.out.println("buffer = " + buff);
      buff.append(true);
      System.out.println("After append = " + buff);
      buff = new StringBuffer("abcd ");
      System.out.println("buffer = " + buff);    
      buff.append(false);
      System.out.println("After append = " + buff);
	// ensureCapacity
	  StringBuffer buff1 = new StringBuffer("tuts point");
      System.out.println("buffer1 = " + buff1);
      System.out.println("Old Capacity = " + buff1.capacity());
      buff1.ensureCapacity(28);
      System.out.println("New Capacity = " + buff1.capacity());
      StringBuffer buff2 = new StringBuffer("compile online");
      System.out.println("buffer2 = " + buff2);
      System.out.println("Old Capacity = " + buff2.capacity());
      buff2.ensureCapacity(29);
      System.out.println("New Capacity = " + buff2.capacity());
	// charAt and SetCharAt
	  System.out.println("The character at indes 3 is" + buff1.charAt(3));
	  buff1.setCharAt(3,'X');
	  System.out.println("The new String is " + buff1);
	// reverse
	  System.out.println("Normal String is " + buff1);
	  System.out.println("Reversed String is " + buff1.reverse());
	// delete and deleteCharAt
	  System.out.println("The original String is "+ buff1);
	  System.out.println("String with 3rd character deleted is " + buff1.deleteCharAt(3));
	  System.out.println("String with 3 character deleted is " + buff1.delete(1,4));
	// replace
	  System.out.println("Original String is"+ buff);
	  buff.replace(1,5,"damn");
	  System.out.println("String with replaced text is" + buff);
   }
}