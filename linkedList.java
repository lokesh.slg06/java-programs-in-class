import java.util.*;  
public class linkedList {  
 public static void main(String args[]){  
  
  LinkedList<String> al=new LinkedList<String>();
  System.out.println("Adding Ravi to the linked list");
  al.add("Ravi");
  System.out.println("Adding Vijay to the linked list");
  al.add("Vijay");  
  System.out.println("Adding Sanjay to the linked list");
  al.add("Sanjay");  
  System.out.println("Adding Ajay to the linked list");
  al.add("Ajay");  
  System.out.println("The contents of the the list are:\n");
  Iterator<String> itr=al.iterator();  
  while(itr.hasNext()){  
   System.out.println(itr.next());  
  }  
 }  
}  