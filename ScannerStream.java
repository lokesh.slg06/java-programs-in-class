import java.io.*;
import java.util.Scanner;

public class ScannerStream 
{
	public static void main(String args[])
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter another number");
		int number1=sc.nextInt();
		System.out.println("Data is "+number1+" is successfully read from the console");
	}
}
