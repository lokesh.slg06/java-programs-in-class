import java.util.*;
import java.io.*;

class Student{
	public String name;
	public float attendance;
	public String RollNo;
	public static int ClassRoomCode;
	
	
	public void TakeDetails(String Name,String RollNo,float attendance){
		System.out.println("Inputting details");
		this.name=Name;
		this.RollNo=RollNo;
		this.attendance=attendance;
	}
	
	public void displayDetails(){
		System.out.println("Name: " + this.name);
		System.out.println("Attendance: " + this.attendance);
		System.out.println("RollNo: " + this.RollNo);
		System.out.println("ClassRoomCode: "+ ClassRoomCode);
	}
}
public class database{
	public static void main(String args[])throws IOException{
			Scanner sc=new Scanner(System.in);
			int cc;
			System.out.println("Enter ClassRoom Code for the database entry");
			cc=sc.nextInt();
			File file=new File("database.txt");
			BufferedWriter output = new BufferedWriter(new FileWriter(file));
			// boolean condition=true;
			Student s=new Student();
			s.ClassRoomCode=cc;
			System.out.println("Enter name");
			String name=sc.nextLine();
			System.out.println("Enter Roll No");
			String RollNo=sc.nextLine();
			System.out.println("Enter Attendance");
			float att=sc.nextFloat();
			s.TakeDetails(name,RollNo,att);
			try{
				output.write(s.name);
				output.write(s.RollNo);
				// System.out.println(s.RollNo);
			}catch(IOException e){
				e.printStackTrace();
			} finally {
				if(output!= null){
					output.close();
				}
			}
			System.out.println("The data entered is :");
			System.out.println(s.name + "\n" + s.attendance + "\n" + s.RollNo + "\n" + s.ClassRoomCode);
		}
	}