import java.util.*;
import java.io.*;

class SampleConstructorOverloading {
	int x;
	SampleConstructorOverloading(){
		this.x=10;
	}
	SampleConstructorOverloading(int y){
		this.x=y;
	}
	void display(){
		System.out.println("Value of x is" + this.x);
	}
}

class ConstructorOverloading {
	
}