import java.util.*;
import java.io.*;

class StringSample {
  public static void main(String args[]) {
    String alphabet = "";	
	String alpha = "";	
	 String s = "founder of diginext magazine";
	 System.out.print("Upper case           :: ");
	 System.out.println(s.toUpperCase());
	 System.out.print("Lower case           :: ");
	 System.out.println(s.toLowerCase());
	 System.out.print("trimming             :: ");
	 System.out.println(s.trim());
	 Scanner a=new Scanner(System.in);
	 System.out.print("Length of String is  ::");
	 System.out.println(s.length());
	 System.out.print("enter the position to get alphabet of  ::");
	Scanner b=new Scanner(System.in);
	 int k=b.nextInt();
	 System.out.println(s.charAt(k-1));
	//  System.out.println(" new string data  ::"+s);
	 Scanner c=new Scanner(System.in);
	 System.out.print(" you want to replace 'founder' with  :: ");
	 String con=System.console().readLine();
	 String replaceString=s.replace("founder",con);
	  System.out.println(replaceString);
	 StringBuffer sb=new StringBuffer("Yaayyy ");
	 sb.append(" Cool");
	 System.out.println(sb);
	 System.out.print("change   :: ");
	 sb.insert(0," :p ");
	 System.out.println(sb);
	 System.out.print("delete  (1,3) :");
	 sb.delete(1,3);
	 System.out.println(sb);
	 String text=new String("Life is a journey");
	 System.out.println("The new string is "+text);
	 if(!s.equalsIgnoreCase(text)){
		System.out.println("They're not the same");
	 }
	 String testRegion=new String("Lifeug");
	 if(text.regionMatches(0,testRegion,0,3)){
		 System.out.println("The region matches");
	 }else{
		 System.out.println("The region doesn't match");
	 }
	 boolean textStart =text.startsWith("Life",4);
	 System.out.println(testRegion + " starts with Life?\n" + textStart);
	 String texttest="Hello";
	 System.out.println("The characters after 1 are\n");
	 System.out.println(texttest.substring(1));
  }
}
