public class genericClasses {
   // generic method printArray
   public static < E > int printAndCountArray( E[] inputArray ) {
      // Display array elements
	  int x=0;
      for(E element : inputArray) {
         System.out.printf("%s ", element);
         x++;
      }
      System.out.println("\nThe number of elements this array has are:");
      return x;
   }

   public static void main(String args[]) {
      // Create arrays of Integer, Double and Character
      Integer[] intArray = { 123121, 2343142, 31312, 44234, 25 };
      Double[] doubleArray = { 11.3241, 52.2, 23.3, 40.4 };
      Character[] charArray = { 'H', 'E', 'L', 'L', 'O' };
    //   System.out.println("Array integerArray contains:");
      System.out.println(printAndCountArray(intArray));   // pass an Integer array
    //   System.out.println("\nArray doubleArray contains:");
      System.out.println(printAndCountArray(doubleArray));   // pass a Double array
    //   System.out.println("\nArray characterArray contains:");
      System.out.println(printAndCountArray(charArray));   // pass a Character array
   }
}