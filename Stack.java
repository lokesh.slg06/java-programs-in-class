import java.util.Scanner;
import java.io.*;

class Stack_DataSet
{
	int[] data=new int[10];
	int top;
	
	Stack_DataSet()
	{
		top=-1;
	}
	
	void push(int n)
	{
		if(top==10)
		{
			System.out.println("Stack overflow");
		}else
		{
			data[++top]=n;
		}
	}
	
	void pop()
	{
		if(top==-1)
		{
			System.out.println("Stack underflow");
		}
		else
		{
			top--;
		}
	}
	
	void display()
	{
		for(int i=0;i<=top;i++)
		{
			System.out.print(data[i] + "   ");
		}
		System.out.println(" ");
	}
}

public class Stack
{
	public static void main(String args[])
	{
		Scanner sc=new Scanner(System.in);
		Stack_DataSet s=new Stack_DataSet();
		int number;
		int looper=1;
		int ch;
		while(looper==1)
		{
			System.out.println("1. Push");
			System.out.println("2. Pop");
			System.out.println("3. Display");
			System.out.println("4. Exit");
			System.out.println("Enter Choice");
			ch=sc.nextInt();
			switch(ch)
			{
				case 1:
					System.out.println("Enter number");
					number=sc.nextInt();
					s.push(number);
					break;
				case 2:
					s.pop();
					break;
				case 3:
					s.display();
					break;
				case 4:
					looper=0;
					break;
			}
		}
	}
}