import java.util.*;
import java.io.*;

class Queue_DataSet
{
	int [] dataSet=new int[10];
	int rear,front;
	
	Queue_DataSet()
	{
		rear=front=-1;
	}
	
	void Enquqe(int n)
	{
		if(front==10)
			System.out.println("Queue Overflow");
		else
			dataSet[++front]=n;
	}
	
	void Dequque()
	{
		if(front==rear)
			System.out.println("Queue Underflow");
		else
			++rear;
	}
	
	void display()
	{
		for(int i=rear;i<=front;i++)
			System.out.print(dataSet[i]+"   ");
		System.out.println(" ");
	}
}

public class Queue
{
	public static void main(String args[])
	{
		Scanner sc=new Scanner(System.in);
		Queue_DataSet s=new Queue_DataSet();
		int number;
		int looper=1;
		int ch;
		while(looper==1)
		{
			System.out.println("1. Push");
			System.out.println("2. Pop");
			System.out.println("3. Display");
			System.out.println("4. Exit");
			System.out.println("Enter Choice");
			ch=sc.nextInt();
			switch(ch)
			{
				case 1:
					System.out.println("Enter number");
					number=sc.nextInt();
					s.Enquqe(number);
					break;
				case 2:
					s.Dequque();
					break;
				case 3:
					s.display();
					break;
				case 4:
					looper=0;
					break;
			}
		}
	}
}